import React, { Component } from 'react';
import { Provider } from 'react-redux';

import store from './app/store'; //Import the store
import MainContainer from './app/components/MainContainer';



export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <MainContainer />
            </Provider>
        );
    }
}




