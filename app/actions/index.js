import { AsyncStorage } from 'react-native';

import { checkDateOfflineMode, checkTodayDate, fetchTimeout } from '../helpers';

export const DATA_AVAILABLE = 'DATA_AVAILABLE';
export const GET_USER = 'GET_USER';
export const CONNECTION = 'CONNECTION';
export const CLEAR_TOOLBAR = 'CLEAR_TOOLBAR';
export const DROPDOWN_DATA = 'DROPDOWN_DATA';

export const FACEBOOK_CACHED = 'FACEBOOK_CACHED';
export const CHECK_FACEBOOK = 'CHECK_FACEBOOK';

export const GOOGLE_CACHED = 'GOOGLE_CACHED';
export const CHECK_GOOGLE = 'CHECK_GOOGLE';

export const NO_OFFLINE_DATA = 'NO_OFFLINE_DATA';

export const SHOW_CHOOSE_CITY = 'SHOW_CHOOSE_CITY'; 



function makeRequest(user, id, callback, errorCallback) {
    console.log("Actions makeRequest user " + user);
    let otherDaysUrl = `http://api.openweathermap.org/data/2.5/forecast?id=${id}&APPID=465db2d5bb1bb8b40393a6ec0691a363`;

    function parseData(response) {
        let data = response;

        console.log("DATA TEST");
        console.log(data);

        callback(data);

        const lastCityQuery = user + ':lastCityId';
        const dataQuery = user + ':data';
        
        AsyncStorage.setItem(lastCityQuery, id + "");

        AsyncStorage.getItem(dataQuery)
        .then((container) => {
            let dataToSave = {};
            if(container) {
                dataToSave = JSON.parse(container);
                dataToSave[id] = data;
            }
            else {
                dataToSave[id] = data;
            }

            AsyncStorage.setItem(dataQuery, JSON.stringify(dataToSave));
        })  
    }

    fetch(otherDaysUrl).then((response) => {
        console.log("SUCESS REQUSET from actions");
        return response.json();
    }).then((res) => {
        parseData(res);
    }).catch((error) => {
        console.log("Network error " + error );
        errorCallback();   
    })
}

export function getData(user, id, connection) {
    return (dispatch) => {
        console.log("GetData connection " + connection);
        if(!connection) {
            const lastCityQuery = user + ':lastCityId';
            checkLocalData(user, id, () => {
                dispatch({ type: NO_OFFLINE_DATA });
            }, 
            () => { console.log("Couldn't make request cause no internet")},
             (validData) => {
                console.log("SUCESS DISPATCHED FROM ASYN STORAGE...");
                AsyncStorage.setItem(lastCityQuery, id + "");
                dispatch({ type: DATA_AVAILABLE, data: validData, lastCityId: id });
            }, connection);
        }
        else {
            console.log("Actions getData user " + user);
            const lastCityQuery = user + ':lastCityId';
            checkLocalData(user, id, () => {
                makeRequest(user, id, (data) => { // callback hell :(
                    dispatch({ type: DATA_AVAILABLE, data: data, lastCityId: id });
                }, () => dispatch({ type: NO_OFFLINE_DATA }) )
            }, false, (validData) => {
                console.log("SUCESS DISPATCHED FROM ASYN STORAGE...");
                AsyncStorage.setItem(lastCityQuery, id + "");
                dispatch({ type: DATA_AVAILABLE, data: validData, lastCityId: id });
            }, connection);
        }    
    };
}

function logWrongRecord() {
    console.log("Wrong record, lastCityId exists, but no data");
}

export function getInitialState(connection, user) {
    return (dispatch) => {
        try {
            // AsyncStorage.removeItem('anonym:lastCityId');
            // AsyncStorage.removeItem('anonym:data');
            // AsyncStorage.getAllKeys().then((keys) => {
            //     console.log(keys);
            //     AsyncStorage.multiRemove(keys);
            // });
            console.log("ACTIONS USER");
            console.log(user);
            const query = user + ':lastCityId';
            AsyncStorage.getItem(query)
                .then((item) => {
                    if(item) {
                        checkLocalData(user, item, () => {
                            makeRequest(user, item, (data) => {
                                dispatch({ type: DATA_AVAILABLE, data: data, lastCityId: item });
                            }, () => dispatch({ type: NO_OFFLINE_DATA }) )
                        }, logWrongRecord, (validData) => {
                            console.log("Dispatch valid data from store");
                            dispatch({ type: DATA_AVAILABLE, data: validData, lastCityId: item });
                        } , connection)
                    }
                    else {
                        console.log("First App StartUp or no user experience");
                        dispatch({ type: SHOW_CHOOSE_CITY, show: true });
                    }
                });
        } catch (err) {
            console.log("AsyncStorage error: " + err);
        }  
    }  
}

function checkLocalData(user, id, callback, logger, getDataCallback, connection) {
        try {
            console.log("Check local data user " + user);
            const query = user + ':data';
            AsyncStorage.getItem(query)
            .then((dataContainer) => {
                if(dataContainer) {
                    const data = JSON.parse(dataContainer);
                    if(data && data[id]) {
                        const validData = checkDataIsValid( data, id, connection );
                        validData ? getDataCallback(validData) : callback && callback();

                    }
                    else {
                        logger && logger();
                        callback && callback();
                    }
                }
                else {
                    logger ? logger() : console.log("First App StartUp or no user experience");
                    console.log("Strange writings....!!");
                    callback && callback();
                }
                
            })
        } catch (error) {
            console.log("checkLocalData AsyncStorage error: " + error);
        }           
}

function checkDataIsValid(data, id, connection) {
    let dispatchData = [];

    if(connection) {
        dispatchData = checkTodayDate(data[id]) ? data[id] : false;
    }
    else {
        console.log("start check offline data ......");
        dispatchData = checkDateOfflineMode(data[id]) ? data[id] : false;
        !dispatchData && console.log("No valid offline data");
    }

    return dispatchData;
}

export function getUser(user) {
    return (dispatch) => {
        dispatch({ type: GET_USER, user: user });
    }
}

export function changeConnection(value) {
    return (dispatch) => {
        console.log("action change connection to " + value);
        dispatch({ type: CONNECTION, data: value });
    }
}

export function clearToolbar(value) {
    return (dispatch) => {
        console.log("action clear Toolbar");
        dispatch({ type: CLEAR_TOOLBAR, data: value });
    }
}

export function dispatchDropdownData(data) {
    return (dispatch) => {
        console.log("action payload dropdown data");
        dispatch({ type: DROPDOWN_DATA, data: data });
    }   
}

export function dispatchFacebook(account) {
    return (dispatch) => {
        console.log("action payload facebook account");
        dispatch({ type: FACEBOOK_CACHED, data: account });
    }
}

export function dispatchGoogle(account) {
    return (dispatch) => {
        console.log("action payload google account");
        dispatch({ type: GOOGLE_CACHED, data: account });
    }
}

export function checkCacheFacebook(value) {
    return (dispatch) => {
        console.log("action payload check cache facebook");
        dispatch({ type: CHECK_FACEBOOK, checked: value });
    }
}

export function checkCacheGoogle(value) {
    return (dispatch) => {
        console.log("action payload check cache google");
        dispatch({ type: CHECK_GOOGLE, checked: value });
    }
}

export function showNoUserExp(value) {
    return (dispatch) => {
        console.log("action payload show no user exp");
        dispatch({ type: SHOW_CHOOSE_CITY, show: value });
    }
}
 




