import { combineReducers } from 'redux';
//import { reducer as network } from 'react-native-offline';
 
import { DATA_AVAILABLE,
         GET_USER,
         CONNECTION,
         CLEAR_TOOLBAR,
         DROPDOWN_DATA,
         FACEBOOK_CACHED,
         GOOGLE_CACHED,
         CHECK_GOOGLE,
         CHECK_FACEBOOK,
         NO_OFFLINE_DATA,
         SHOW_CHOOSE_CITY, 
         } from "../actions/" //Import the actions types constant we defined in our actions


let dataState = { data: [], loading: true, lastCityId: '' };
 
const dataReducer = (state = dataState, action) => {
    switch (action.type) {
        case DATA_AVAILABLE:
            console.log("DATA_AVAILABLE");
            state = Object.assign({}, state, { data: action.data, loading: false, lastCityId: action.lastCityId });
            return state;
        case NO_OFFLINE_DATA:
            console.log("No offline data reducer");
            state = Object.assign({}, state, { loading: true });
            return state;
        default:
            return state;
    }
};

const userReducer = (state = {} , action) => {
    switch (action.type) {
        case GET_USER: 
            state = Object.assign({}, { user: action.user });
            return state;
        default: 
            return state;
    }
}

const connectionReducer = (state = {}, action) => {
    switch (action.type) {
        case CONNECTION:
            console.log("Network reducer works");
            state = Object.assign({}, { data: action.data });
            return state;
        default:
            return state;
    }
}

const toolbarReducer = (state = { data: false }, action) => {
    switch(action.type) {
        case CLEAR_TOOLBAR:
            console.log("toolbar reducer works");
            state = Object.assign({}, { data: action.data });
            return state;
        default:
            return state;
    }
}

const dropdownReducer = (state = {}, action) => {
    switch(action.type) {
        case DROPDOWN_DATA:
            console.log("dropdown reducer works");
            const dropdownData = action.data[0] ? action.data : null;
            console.log(dropdownData);
            state = Object.assign({}, {data: dropdownData });
            return state;
        default:
            return state;
    }
}

// let socialState = { facebook: null, google: null, checkFb: false, checkGoo: false };

const socialCachedReducer = (state = {}, action) => {
    switch(action.type) {
        case FACEBOOK_CACHED:
            console.log("socialCached reducer works");
            state = Object.assign({}, state, { facebook: action.data, checkFb: true });
            console.log(state);
            return state;
        case GOOGLE_CACHED:
            console.log("socialCached reducer works");
            state = Object.assign({}, state, { google: action.data, checkGoo: true });
            console.log(state);
            return state;
        case CHECK_FACEBOOK: 
            console.log("socialCached check_cache case reducer works");
            state = Object.assign({}, state, { checkFb: action.checked });
            console.log(state);
            return state;
        case CHECK_GOOGLE: 
            console.log("socialCached check_cache case reducer works");
            state = Object.assign({}, state, { checkGoo: action.checked });
            console.log(state);
            return state;
        default:
            return state;
    }
}

const firstStartReducer = (state = { show: false }, action) => {
    switch(action.type) {
        case SHOW_CHOOSE_CITY:
            console.log("first start reducer works");
            state = Object.assign({}, { show: action.show });
            return state;
        default:
            return state;
    }
}
 
// Combine all the reducers
const rootReducer = combineReducers({
    dataReducer,
    userReducer,
    connectionReducer,
    toolbarReducer,
    dropdownReducer,
    socialCachedReducer,
    firstStartReducer,
    //network
    // ,[ANOTHER REDUCER], [ANOTHER REDUCER] ....
})
 
export default rootReducer;