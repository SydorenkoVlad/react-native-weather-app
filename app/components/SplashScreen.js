import React from 'react';
import { StyleSheet, View, Image } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions'; //Import your actions

class SplashScreen extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const connection = this.props.isConnected;
        const user = this.props.user;
        this.props.getInitialState(connection, user);
    }

    componentWillUnmount() {
        console.log("Splash unmount");
    }

    render() {
        return (
            <View style={styles.SplashScreen_RootView}>
                <View style={styles.SplashScreen_ChildView}>
 
                    {/* Put all your components Image and Text here inside Child view which you want to show in Splash Screen. */}
                    <Image source={require('../assets/images/noAnimation.png')}
                    style={{width:'70%', height:'70%', resizeMode: 'contain'}} />
                </View>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.dataReducer.loading,
        data: state.dataReducer.data,
        isConnected: state.connectionReducer.data,
        //isConnected: state.network.isConnected,
        user: state.userReducer.user,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);



const styles = StyleSheet.create({
    SplashScreen_RootView: {
        justifyContent: 'center',
        flex:1,
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
    SplashScreen_ChildView: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ccffff',
        flex:1,
        margin: 20,
    }
})