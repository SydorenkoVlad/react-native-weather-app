import React from 'react';
import { StyleSheet, View } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../actions'; //Import your actions

import Toolbar from './Toolbar/Toolbar';

import SplashScreen from '../SplashScreen';
import GeneralContainer from './Weather/GeneralContainer';
import Connection from '../Notifications/Connection';
import LoginPage from '../Auth/LoginPage';
import LastCityHeader from './Weather/LastCityHeader';
import SearchResult from './Toolbar/SearchResult';
import NoUserExp from '../Notifications/NoUserExp';

//import cities from '../cities';

import { searchCity } from '../../helpers';

import { strings } from '../../locales/i18n';



class Home extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        showNoConnection: false,
        showSplashScreen: false,
        showDataView: false,
        lastCity: false,
        lastCityName: '',
        text: false,
        centerElement: strings("homePage.searchCity")
      }
    }

    componentDidUpdate(prevProps) { 
      
      if(prevProps.isConnected !== this.props.isConnected) {
        console.log("Connection status " + this.props.isConnected );
        this.setState({ showNoConnection: !this.props.isConnected });
      }

      if(prevProps.user !== this.props.user) {
          this.setState({ showSplashScreen: true });
          let self = this;

          console.log("User in home " + this.props.user);

          setTimeout(() => {
            self.Hide_Splash_Screen();
          }, 2000);
      }

      if(prevProps.lastCityId !== this.props.lastCityId) {
          const cityName = searchCity(this.props.lastCityId);
          this.setState({ 
            lastCity: this.props.lastCityId,
            lastCityName: cityName,
            showDataView: true,
            centerElement: cityName
          });
      }
    }

    Hide_Splash_Screen = () => {
      this.setState({ 
        showSplashScreen: false, 
      });
    }

   updateDataView = () => {
     console.log("Home updateMethod");
     if(this.props.data) {
        this.setState({
          showDataView: true
        });
     }
   }

   closeNotification() {
      this.setState({ showNoConnection: false });
   }

   dispatchToDropdown = (text) => {
     console.log("dispatchToDropdown");
      this.setState({ text: text });
   }

    render() {

      if(!this.props.user) {
        return (
          <LoginPage />
        );
      }

      if(this.state.showSplashScreen) {
        return (
          <View style={styles.splashContainer}>
            <SplashScreen />
          </View>
        )
      }

      
      else {
        return (
          <View style={styles.homeContainer}>
            <View style={styles.toolbarContainer}>
                <Toolbar dispatchToDropdown={this.dispatchToDropdown.bind(this)} updateView={this.updateDataView.bind(this)}/>
            </View>
            {this.state.text && <SearchResult text={this.state.text} loading={this.props.loading}/>}
            {this.state.lastCity && !this.props.loading && <LastCityHeader city={this.state.lastCityName}/>}
            {this.props.noUserExp && <NoUserExp />}
            {this.state.showDataView && <GeneralContainer />}
            {this.state.showNoConnection && <Connection data={this.state.showDataView} close={this.closeNotification.bind(this)}/>}
          </View>
        );
      } 
    }
  }

  // The function takes data from the app current state,
// and insert/links it into the props of our component.
// This function makes Redux know that this component needs to be passed a piece of the state
function mapStateToProps(state, props) {
    return {
        loading: state.dataReducer.loading,
        data: state.dataReducer.data,
        lastCityId: state.dataReducer.lastCityId,
        user: state.userReducer.user,
        isConnected: state.connectionReducer.data,
        dropdownData: state.dropdownReducer.data,
        noUserExp: state.firstStartReducer.show,
        //isConnected: state.network.isConnected,
    }
  }
  
  // Doing this merges our actions into the component’s props,
  // while wrapping them in dispatch() so that they immediately dispatch an Action.
  // Just by doing this, we will have access to the actions defined in out actions file (action/home.js)
  function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
  }
  
  //Connect everything
  export default connect(mapStateToProps, mapDispatchToProps)(Home);

  const styles = StyleSheet.create({
    homeContainer: {
      flex: 1,
      backgroundColor: 'white'
    },
    splashContainer: {
      flex: 1,
      backgroundColor: '#ccffff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    toolbarContainer: {
      alignItems: 'center'
    },
    dropdown: {
      flex: 1,
      width: '100%'
    }
  });