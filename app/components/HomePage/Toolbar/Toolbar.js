import React from 'react';
import { TouchableOpacity, Platform } from 'react-native';
import { Header, Item, Input, Icon, Button, Text } from 'native-base';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../../actions'; //Import your actions

import { searchCity, fetchTimeout } from '../../../helpers';
import { strings } from '../../../locales/i18n';


class Toolbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ''
        }
    }

    componentDidUpdate() {
        if(this.props.needClearToolbar) {
            this.clearSearchBar();
        }
    }

    callDataRequest(connection) {
        console.log("Toolbar ping " + connection)

        const user = this.props.user;
        const id = searchCity(null, this.state.text);

        if(id) {
            this.props.noUserExp && this.props.showNoUserExp(false);
            this.props.getData(user, id, connection);
            this.props.updateView();
        }
  
        this.props.clearToolbar(true);
    }

    getData = () => {

        fetchTimeout('http://google.com', 2000)
            .then(() => {
                console.log("component ping.....");
                const connection = true;

                this.callDataRequest(connection);
                this.props.changeConnection(connection);
            })
            .catch((e) => {
                console.log("component ping Error " + e);
                const connection = false;

                this.callDataRequest(connection);
                this.props.changeConnection(connection);
            })
    } 

    clearSearchBar = (byUser) => {
        if(byUser || this.props.needClearToolbar) {
            this.props.clearToolbar(false);
            this.props.dispatchToDropdown();

            console.log("clear toolbar");

            this.setState({
                text: ''
            });
        }
    }

    handleInput = (text) => {
        if(text) {
            this.props.dispatchToDropdown(text);
        }
        this.setState({
            text: text
        });
    }

    render() {
        const byUser = true;

        return (
            <Header searchBar rounded style={{width: '100%'}}>
                <Item>
                    <Icon name="ios-search" />
                    
                    <Input 
                        placeholder={strings('homePage.searchCity')}
                        value={this.state.text}
                        onChangeText={(value) => this.handleInput(value)}
                        onSubmitEditing={() => this.getData()}/>

                    <TouchableOpacity onPress={() => this.clearSearchBar(byUser)}>
                      <Icon name="ios-close"/>
                    </TouchableOpacity>

                  </Item>

                  {Platform.OS === "android" && 
                  <Button transparent>
                    <Text>{strings('homePage.search')}</Text>
                 </Button> }

            </Header>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        dispatchToDropdown: props.dispatchToDropdown,
        updateView: props.updateView,
        needClearToolbar: state.toolbarReducer.data,
        isConnected: state.connectionReducer.data,
        user: state.userReducer.user,
        data: state.dataReducer.data,
        noUserExp: state.firstStartReducer.show,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(Toolbar);