import React from 'react';
import { View } from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../../../actions'; //Import your actions

import city1 from '../../../dataFolder/cities1';
import city2 from '../../../dataFolder/cities2';
import city3 from '../../../dataFolder/cities3';
import city4 from '../../../dataFolder/cities4';
import city5 from '../../../dataFolder/cities5';
import city6 from '../../../dataFolder/cities6';
import city7 from '../../../dataFolder/cities7';
import city8 from '../../../dataFolder/cities8';
import city9 from '../../../dataFolder/cities9';
import city10 from '../../../dataFolder/cities10';

import Dropdown from './Dropdown';
import NotFound from '../../Notifications/NotFound';


class SearchResult extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.changeItemsList();
    }

    componentDidUpdate(prevProps) {
        if(prevProps.searchedText !== this.props.searchedText) {
            this.changeItemsList();       
        }
    }

    changeItemsList() {
        const searchedText = this.props.searchedText;

        let data = [city1, city2, city3, city4, city5,
            city6, city7, city8, city9, city10];

        let citiesArray = [];

        for(let i = 0; i < data.length; i++) {
            
            let list = data[i].filter( (item) =>  {
                if(item.name.toLowerCase().indexOf(searchedText.toLowerCase()) == 0) return true;
                //return item.name.toLowerCase().indexOf(searchedText.toLowerCase()) > -1;
                return false;
              });
    
            if(list) {
                citiesArray = citiesArray.concat(list);
            }
            
        }

        this.props.dispatchDropdownData(citiesArray);
    }

    render() {
        let Result;
        if(this.props.dropdownData) {
            Result = <Dropdown />
        }
        else Result = <NotFound />
        return (
            <View>{Result}</View>            
        );
    }
}

function mapStateToProps(state, props) {
    return {
        dropdownData: state.dropdownReducer.data,
        searchedText: props.text,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(SearchResult);