import React from 'react';

import { ScrollView, SectionList, View, StyleSheet } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../../actions'; //Import your actions

import DropdownItem from './DropdownItem';


class Dropdown extends React.Component {
    constructor(props) {
        super(props);
    }

    getCities = () => {
        const cityNames = [];
        const cities = this.props.dropdownData;

        for(let index = 0; index < cities.length; index++) {
            const city = cities[index].name;
            const id = cities[index].id;

            const obj = {};
            const arr = []; 
            arr.push( <DropdownItem key={index} id={id} city={city} /> );
            obj.data = arr;
            cityNames.push(obj);            
        }

        const scrollableView = (<SectionList
            scrollEnabled
            animated={false}
            renderItem={({item, index}) => <View key={index}>{item}</View>}
            sections={cityNames}
            keyExtractor={(index) => index}
            keyboardShouldPersistTaps="always"
        />);

        return scrollableView;
    }

    render() {
        const data = this.getCities();

        return (
            <ScrollView style={styles.container} keyboardShouldPersistTaps="always">
                {data}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        maxHeight: 145,
    },
})

function mapStateToProps(state, props) {
    return {
        searchedText: props.text,
        updateMethod: props.updateMethod,
        dropdownData: state.dropdownReducer.data,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Dropdown);

