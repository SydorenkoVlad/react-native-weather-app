import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../../actions'; //Import your actions

import { fetchTimeout } from '../../../helpers';

import { TouchableOpacity, Text, View, StyleSheet, Keyboard } from 'react-native';


class DropdownItem extends React.Component {
    constructor(props) {
        super(props);
    }

    callDataRequest(connection) {
        Keyboard.dismiss();
        console.log("DropdownItem ping " + connection)

        const user = this.props.user;
        const id = this.props.id;

        this.props.noUserExp && this.props.showNoUserExp(false);

        this.props.getData(user, id, connection);

        this.props.clearToolbar(true);
    }

    getGeneralData = () => {

        fetchTimeout('http://google.com', 2000)
            .then(() => {
                console.log("component ping.....");
                const connection = true;

                this.props.changeConnection(connection);
                this.callDataRequest(connection);
            })
            .catch((e) => {
                console.log("component ping Error " + e);
                const connection = false;

                this.props.changeConnection(connection);
                this.callDataRequest(connection);
            })
    } 

    render() {
        return (
        <View style={styles.itemContainer}>
            <TouchableOpacity onPress={() => this.getGeneralData()}>
                <View style={styles.item}>
                    <Text>{this.props.city}</Text>
                </View>
            </TouchableOpacity>
        </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        city: props.city,
        id: props.id,
        isConnected: state.connectionReducer.data,
        //isConnected: state.network.isConnected,
        user: state.userReducer.user,
        updateMethod: props.updateMethod,
        data: state.dataReducer.data,
        noUserExp: state.firstStartReducer.show,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(DropdownItem);

const styles = StyleSheet.create({
    item: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 15
    },
    itemContainer: {
        backgroundColor: '#D2E4F8',
        borderBottomWidth: 0.5,
        borderColor: 'black',
        borderStyle: 'solid',
    }
})

