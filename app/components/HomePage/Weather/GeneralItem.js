import React from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

import { convertTempreature } from '../../../helpers';

import WeatherContainer from './WeatherContainer';
import AdditionalInfo from './Additional/AdditionalInfo';

import { strings } from '../../../locales/i18n';


const monthNames = [strings('months.january'), strings('months.february'),
strings('months.march'), strings('months.april'), strings('months.may'),
strings('months.june'), strings('months.july'), strings('months.august'),
strings('months.september'), strings('months.october'), strings('months.november'),
strings('months.december')
];

const weekDays = {
    "Mon": strings('daysOfWeek.monday'),
    "Tue": strings('daysOfWeek.tuesday'),
    "Wed": strings('daysOfWeek.wednesday'),
    "Thu": strings('daysOfWeek.thursday'),
    "Fri": strings('daysOfWeek.friday'),
    "Sat": strings('daysOfWeek.saturday'),
    "Sun": strings('daysOfWeek.sunday'),
}
    



class GeneralItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showMore: false
        }
    }

    showAdditional = () => {
        this.setState({
            showMore: !this.state.showMore
        });
    }

    render() {
        const itemKey = this.props.itemNum;
        const todayItem = !itemKey; 

        /* Parse API data block */
        let data = this.props.data.list ? this.props.data.list[itemKey] : '';
        let dateTxt = this.props.data.list ? this.props.data.list[itemKey].dt_txt : '';
        let date, weekDay, formatedDate, tempreature, icon;
        date = weekDay = formatedDate = tempreature = icon = '';
        
        if(data) {
            date = new Date(data.dt * 1000);
            weekDay = weekDays[ date.toString().split(' ')[0] ]; 
            formatedDate = monthNames[date.getMonth()] + "   " + date.getDate();

            tempreature = convertTempreature(data.main.temp);
            icon = data.weather[0].icon;
        }

        return (
        <View>
            <TouchableOpacity onPress={() => this.showAdditional()}>
                <View style={ todayItem ? styles.todayContainer : styles.container}>
                    <View style={styles.infoContainer}>

                        <WeatherContainer data={weekDay} styles={styles.alignedItem} />
                        <WeatherContainer data={formatedDate} styles={styles.dateAlign} />
                        <WeatherContainer data={tempreature} styles={styles.alignedItem} />  

                        <View style={styles.alignedItem}>
                            <Image style={{width: 50, height: 50}} source={{uri: `http://openweathermap.org/img/w/${icon}.png`}} />
                        </View>

                    </View>
                </View>
                </TouchableOpacity>
                {this.state.showMore && <AdditionalInfo date={dateTxt}/>}
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.dataReducer.loading,
        data: state.dataReducer.data,
        itemNum: props.itemNum
    }
}

export default connect(mapStateToProps)(GeneralItem);


const styles = StyleSheet.create({
    todayContainer: {
        margin: 12,
        borderColor: '#0000FF',
        borderWidth: 2,
        borderRadius: 5,
        backgroundColor: 'white',
    },
    container: {
        margin: 10,
        borderColor: '#6495ED',
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: 'white',
    },
    infoContainer: {
        flex: 1,
        flexDirection: 'row',
        margin: 10
    },
    alignedItem: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
    },
    dateAlign: {
        flex: 2,
        justifyContent: 'center',
        alignContent: 'center',
    }
});