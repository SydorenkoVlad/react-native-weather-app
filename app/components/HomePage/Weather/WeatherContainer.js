import React from 'react';
import { View, Text } from 'react-native';

class WeatherContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={this.props.styles}>
                <Text>{this.props.data}</Text>
            </View>
        );
    }
}

export default WeatherContainer;