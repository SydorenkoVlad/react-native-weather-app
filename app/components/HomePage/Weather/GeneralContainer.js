import React from 'react'
import { View, StyleSheet, ActivityIndicator, SectionList, ScrollView } from 'react-native';
import { connect } from 'react-redux';

import GeneralItem from './GeneralItem';
import NoOfflineData from '../../Notifications/NoOfflineData';


class GeneralContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    renderLoader = () => {
        return(
            <View style={styles.loaderContainer}>
                <ActivityIndicator size="large" color="#0000ff" /> 
            </View>
        );
    }
        
    
    render() {

        const timeStamps = [];
        const timeline = 8;

        let timeStamp = 0;

        for(let index = 0; index < 5; index++) {
            timeStamp += index ? timeline : index;
            timeStamps.push(<GeneralItem key={index} itemNum={timeStamp}/>);
        }

        const scrollableView = (<SectionList
            scrollEnabled
            animated={false}
            renderItem={({item, index}) => <View key={index}>{item}</View>}
            sections={[
                {data: [ timeStamps[0] ] },
                {data: [ timeStamps[1] ] },
                {data: [ timeStamps[2] ] },
                {data: [ timeStamps[3] ] },
                {data: [ timeStamps[4] ] },   
            ]}
            keyExtractor={(index) => index}
        />);

        let renderData;

        if(!this.props.loading) renderData = scrollableView;
        else if(this.props.loading && !this.props.isConnected) renderData = <NoOfflineData />
        else renderData = this.renderLoader();

        return (
            <ScrollView style={{flex: 1}}>
                {renderData} 
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    loaderContainer: {
        margin: 30
    }
});

function mapStateToProps(state, props) {
    return {
        loading: state.dataReducer.loading,
        data: state.dataReducer.data,
        isConnected: state.connectionReducer.data,
    }
}

export default connect(mapStateToProps)(GeneralContainer);



