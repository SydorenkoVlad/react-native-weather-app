import React from 'react';
import { View } from 'react-native';

import TimeStampHeader from './TimeStampHeader';
import TimeStampMain from './TimeStampMain';


export default class TimeStamp extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View key={this.props.time}>
                <TimeStampHeader icon={this.props.data.icon} time={this.props.time} />
                <TimeStampMain data={this.props.data} />
            </View>
        );
    }
}