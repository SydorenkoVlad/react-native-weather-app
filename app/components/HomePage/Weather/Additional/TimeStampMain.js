import React from 'react';
import { View, StyleSheet } from 'react-native';

import TimeStampRow from './TimeStampRow';

export default class TimeStampMain extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { data } = this.props;
        const rows = [];

        for(let key in data) {
            if(data[key] && data[key] !== "" && key != 'icon') {
                rows.push(<TimeStampRow key={key} name={key} data={data[key]} />);
            }
        }

        return (
            <View style={styles.container}>
                {rows}
            </View>
        );
    }
}

const styles = StyleSheet.create({
   container: {
       flex: 1,
    //    alignContent: 'center',
    //    justifyContent: 'center',
       borderBottomWidth: 2,
       borderBottomColor: 'black'
   }
});
