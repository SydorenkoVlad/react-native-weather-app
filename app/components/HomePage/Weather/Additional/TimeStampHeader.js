import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

export default class TimeStampHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { icon, time } = this.props;

        return (
            <View style={styles.header}>
                <View style={styles.imageContainer}>
                    <Image source={require('../../../../assets/images/clock.png')} style={styles.img} />
                </View>
                 <View style={styles.time}>
                     <Text>{time}</Text>
                </View>
                <View style={styles.imageContainer}>
                    <Image source={{uri: icon}} style={styles.img} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
   header: {
       flex: 1,
       flexDirection: 'row',
       alignContent: 'center',
       justifyContent: 'center',
       borderBottomWidth: 2,
       borderBottomColor: '#D2E4F8',
       margin: 15,
       
   },
   img: {
       width: 40,
       height: 40
   },
   imageContainer: {
       justifyContent: 'center',
       alignContent: 'center'
   },
   time: {
       justifyContent: 'center',
       alignContent: 'center',
       marginLeft: 10,
       marginRight: 10
   }
});
