import React from 'react';
import { View, StyleSheet, SectionList } from 'react-native';
import { connect } from 'react-redux';

import TimeStamp from './TimeStamp';

import { parseAdditionalInfo } from '../../../../helpers';

class AdditionalInfo extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
       const weatherArray = this.props.data;
       
       let requestDay = this.props.date;
       const month = requestDay.substr(5, 2);
       requestDay = month + "." + requestDay.substr(8, 2);

       let data = parseAdditionalInfo(weatherArray, requestDay);

       let timeStamps = [];
       const day = data[requestDay];

       for(let time in day) {
           let elem = [];
           elem.push(<TimeStamp data={day[time]} time={time} />);
           timeStamps.push({ data: elem });
       }

        return (
            <View style={styles.container}>
                <SectionList
                    scrollEnabled
                    renderItem={({item, index}) => <View style={styles.infoContainer}>{item}</View>}
                    sections={timeStamps}
                    keyExtractor={(index) => index + requestDay}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginLeft: 10,
        marginRight: 10,

        flex: 1,

        borderColor: '#D2E4F8',
        borderLeftWidth: 10,
        borderRightWidth: 10,
        borderBottomWidth: 10,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        
        backgroundColor: 'white'
    },
    infoContainer: {
        margin: 5,
        justifyContent: 'center',
        alignContent: 'center'
    }
})

function mapStateToProps(state) {
    return {
        loading: state.dataReducer.loading,
        data: state.dataReducer.data
    }
}

export default connect(mapStateToProps)(AdditionalInfo);
