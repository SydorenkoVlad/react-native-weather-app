import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import { strings } from '../../../../locales/i18n';

export default class TimeStampRow extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { name, data } = this.props;

        return (
            <View style={styles.container}>
                <Text style={styles.text}>{strings(`weatherConditions.${name}`)}</Text>
                <Text style={styles.text}>{data}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
   container: {
       flex: 1,
       flexDirection: 'row',
       margin: 5,
       alignContent: 'center',
       justifyContent: 'center'
   },
   text: {
       flex: 1
   }
});
