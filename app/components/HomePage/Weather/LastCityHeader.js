import React from 'react'

import { View, StyleSheet, Text } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { connect } from 'react-redux';



class LastCityHeader extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={styles.container}>
                <Icon name="city" size={30}/>
                <Text style={styles.textContainer}>{this.props.city}</Text>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        city: props.city
    }
}

export default connect(mapStateToProps)(LastCityHeader);

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        maxHeight: 100,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20,
    },
    textContainer: {
        margin: 10,
        fontSize: 35,
    }
})

