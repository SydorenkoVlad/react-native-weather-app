import React from 'react';
import { StyleSheet, View, Image, Text, TouchableOpacity } from 'react-native';
import { LoginButton, AccessToken, GraphRequestManager, GraphRequest } from 'react-native-fbsdk';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../actions'; //Import your actions

class FacebookButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            silentLogin: null,
            cachedAccount: null,
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.isConnected !== this.props.isConnected && this.props.isConnected) {
            this.facebookSilentLogin();
        }

        if(prevProps.facebookCached !== this.props.facebookCached && this.props.facebookCached) {
            this.setState({ cachedAccount: this.props.facebookCached });
        }
    }

    facebookLogin = (error, result, loginType) => {
        if (error) {
            console.log("Facebook login error: " + error);
        } else if (result && result.isCancelled) {
            console.log("Facebook login: Cancelled ");
        } else {
            console.log("Facebook login: SUCESS");
            AccessToken.getCurrentAccessToken().then(
                (data) => {

                  let accessToken = data ? data.accessToken : null;

                  if(!accessToken) {
                      return;
                  } 

                  const responseInfoCallback = (error, result) => {
                    if (error) {
                      console.log('Error fetching data: ' + error);
                    } else {
                      console.log('Success fetching data: ' + JSON.stringify(result));
                      this.setState({ silentLogin: result.email });
                      loginType === 'user' && this.props.getUser("facebook " + result.email);
                    }
                  }

                  const infoRequest = new GraphRequest(
                    '/me',
                    {
                      accessToken: accessToken,
                      parameters: {
                        fields: {
                          string: 'email'
                        }
                      }
                    },
                    responseInfoCallback
                  );
      
                  // Start the graph request.
                  new GraphRequestManager().addRequest(infoRequest).start();
      
                })
        }
    }

    facebookSilentLogin = () => {
        try {
            this.facebookLogin();
        } catch(error) {
            console.log("Silent facebook login error " + error);
        }
    }

    facebookUserLogin = (error, result) => {
        const type = "user";
        this.facebookLogin(error, result, type);
    }

    logout = () => {
        this.setState({ silentLogin: null });
    }

    render() {
        const shortEmail = this.state.silentLogin;

        if(this.props.isConnected) {
            if(this.state.silentLogin) {
                return (
                    <View style={styles.сontainer}>
                        <TouchableOpacity onPress={() => { this.props.getUser("facebook " + shortEmail) }} style={styles.button}>
                            <View style={styles.fbContainer}>
                                <Image source={require('../../assets/images/fb-logo.png')} style={styles.fbLogo} />
                            </View>
                            <View style={styles.textContainer}><Text style={styles.text}>{shortEmail}</Text></View>
                        </TouchableOpacity>
                        <LoginButton
                                readPermissions={["email"]}
                                onLoginFinished={(error, result) => this.facebookUserLogin(error, result)}
                                onLogoutFinished={this.logout}/>
                    </View>
                );
            }
            else {
                console.log("Short email " + shortEmail);
                console.log
                return (
                    <View style={styles.сontainer}>
                        <LoginButton
                            readPermissions={["email"]}
                            onLoginFinished={(error, result) => this.facebookUserLogin(error, result)}
                            onLogoutFinished={this.logout}/>
                    </View>
                );
            }  
        }
        else {
            const shortEmail = this.state.cachedAccount;

            return (
                <View style={cachedAccountStyles.container}>
                    <TouchableOpacity onPress={() => { this.props.getUser("facebook " + shortEmail) }} style={cachedAccountStyles.button}>
                        <View style={cachedAccountStyles.fbContainer}>
                            <Image source={require('../../assets/images/fb-logo.png')} style={cachedAccountStyles.fbLogo} />
                        </View>
                        <View style={cachedAccountStyles.textContainer}><Text style={cachedAccountStyles.text}>{shortEmail}</Text></View>
                    </TouchableOpacity>
                </View> );
        }
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.dataReducer.loading,
        data: state.dataReducer.data,
        lastCityId: state.dataReducer.lastCityId,
        user: state.userReducer.user,
        isConnected: state.connectionReducer.data,
        facebookCached: state.socialCachedReducer.facebook,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(FacebookButton);

const styles = StyleSheet.create({
    сontainer: {
        flex: 1,
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: 190,
        height: 30,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#4167B2',
        margin: 6,
        borderRadius: 4,
      },
      fbContainer: {
        justifyContent: 'flex-start',
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 5,
        marginRight: 5,
      },
      fbLogo: {
        width: 12,
        height: 12,
        margin: 2,
        marginLeft: 4
      },
      textContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 23,
        marginRight: 23,
        marginTop: 5,
      },
      text: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: 'bold',
        color: 'white'
      }
});

const cachedAccountStyles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        width: 190,
        height: 30,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#4167B2',
        margin: 6,
        borderRadius: 4,
    },
    fbContainer: {
        justifyContent: 'flex-start',
        backgroundColor: 'white',
        borderRadius: 2,
        marginLeft: 5,
        marginRight: 5,
    },
    fbLogo: {
        width: 12,
        height: 12,
        margin: 2,
    },
    textContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        // marginLeft: 30,
        // marginRight: 30,
        marginLeft: 20,
        marginRight: 20,
        // marginTop: 8,
        marginTop: 6,
      },
    text: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: 'bold',
        color: 'white'
    }
})

