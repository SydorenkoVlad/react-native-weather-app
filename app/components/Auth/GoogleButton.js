import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Platform,
  Image
} from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../actions'; //Import your actions

import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';

import { fetchTimeout } from '../../helpers';


import { strings } from '../../locales/i18n';


class GoogleButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: null,
      error: null,
    };
  }

  componentDidUpdate(prevProps) {
    if(prevProps.isConnected !== this.props.isConnected && this.props.isConnected) {
      this._configureGoogleSignIn();
      this._getCurrentUser();
    }
  }

  checkNetwork() {
    fetchTimeout('http://google.com', 2000)
      .then(() => {
        console.log("component ping.....");
        const connection = true;
        this.props.changeConnection(connection);
      })
      .catch((e) => {
        console.log("component ping Error " + e);
        const connection = false;
        this.props.changeConnection(connection);
      })
  }

  _configureGoogleSignIn() {
    const configPlatform = {
      ...Platform.select({
        ios: {
          iosClientId: "827359716376-v9lqot61c64s563sf8bsqjmhpeat5cev.apps.googleusercontent.com",
        },
        android: {},
      }),
    };

    //client secret web b93A4roXOksHLJm1UsFCHbai

    GoogleSignin.configure({
      ...configPlatform,
      webClientId: "827359716376-2cvqtt7km8b9ck5apv3mg5l6dmpv32c0.apps.googleusercontent.com", 
      offlineAccess: false,
    });
  }

  async _getCurrentUser() {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      console.log("SUCCESS LOGIN SILENTLY");
      this.setState({ userInfo, error: null });
    } catch (error) {
      console.log("ERROR LOGIN SILENTLY");
      this.setState({
         error,
      });
    }
  }

  render() {
    console.log("first google render");
    console.log("connection");
    console.log(this.props.isConnected);

    console.log("user");
    console.log(this.state.userInfo);

    console.log("Google error....");
    console.log(this.state.error);

    const { userInfo } = this.state;
    if(this.props.isConnected) {
      if (!userInfo) {
        return (
          <View style={styles.container}>
            <GoogleSigninButton
              style={{ width: 212, height: 48 }}
              size={GoogleSigninButton.Size.Standard}
              color={GoogleSigninButton.Color.Auto}
              onPress={this._signIn}
            />
          </View>
        );
      } else {
        const email = userInfo.user.email;
        const shortEmail = email.substring(0, email.indexOf('@'));
        return (
          <View style={styles.container}>
            
            <TouchableOpacity onPress={this._signIn} style={styles.button}>
              <Image source={require('../../assets/images/google.png')} style={{width: 20, height: 20, margin: 5 }} />
              <Text style={styles.text}>{shortEmail}</Text>
            </TouchableOpacity>
  
            <TouchableOpacity onPress={this._signOut} style={styles.button}>
              <Image source={require('../../assets/images/google.png')} style={{width: 20, height: 20, margin: 5 }} />
              <Text style={styles.text}>{strings('login.googleLogout')}</Text>
            </TouchableOpacity>
  
          </View>
        );
      }
    }
    else {
      console.log("account initialize");
      const account = this.props.googleCached;
      const shortEmail = account ? account.substring(0, account.indexOf('@')) : ''; 

      return (
      <View style={styles.container}> 
        <TouchableOpacity onPress={() => this.props.getUser("google " + account)} style={styles.cachedButton}>
          <Image source={require('../../assets/images/google.png')} style={{width: 20, height: 20, margin: 5 }} />
          <Text style={styles.text}>{shortEmail}</Text>
        </TouchableOpacity>
      </View> );
    }
  }

  renderError() {
    const { error } = this.state;
    return (
      !!error && (
        <Text>
          {error.toString()} code: {error.code}
        </Text>
      )
    );
  }

  _signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      this.setState({ userInfo, error: null });
      this.props.getUser("google " + userInfo.user.email);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // sign in was cancelled
        console.log('cancelled');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation in progress already
        console.log('in progress');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('play services not available or outdated');
      } else {
        console.log('Something went wrong', error.toString());
        //Alert.alert('Something went wrong', error.toString());
        this.setState({
          error,
        });
      }
    }
  };

  _signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      console.log("Google sign out");

      this.setState({ userInfo: null, error: null });
    } catch (error) {
        this.checkNetwork();
        
        this.setState({
          error,
        });
    }
  };
}

function mapStateToProps(state, props) {
  return {
      loading: state.dataReducer.loading,
      data: state.dataReducer.data,
      lastCityId: state.dataReducer.lastCityId,
      user: state.userReducer.user,
      isConnected: state.connectionReducer.data,
      googleCached: state.socialCachedReducer.google,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(GoogleButton);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cachedButton: {
    width: 212,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    margin: 10,
    elevation: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  button: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    margin: 10,
    elevation: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  text: {
    flex: 1,
    margin: 5
  }
});