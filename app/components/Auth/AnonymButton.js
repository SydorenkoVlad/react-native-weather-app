import React from 'react';
import { StyleSheet, View, Button } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../actions'; //Import your actions

import { strings } from '../../locales/i18n';


class AnonymButton extends React.Component {
    constructor(props) {
        super(props);
    }
    
    handle_pressButton = () => {
        this.props.getUser("anonym");
    }

    render() {
        return (
            <View style={styles.loginContainer}>
                <Button title={strings('login.anonymous')} onPress={this.handle_pressButton} color='black'/>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        loading: state.dataReducer.loading,
        data: state.dataReducer.data,
        lastCityId: state.dataReducer.lastCityId,
        user: state.userReducer.user,
        isConnected: state.connectionReducer.data,
        //isConnected: state.network.isConnected,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AnonymButton);

const styles = StyleSheet.create({
    loginContainer: {
        flex: 1,
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center'
    }
});