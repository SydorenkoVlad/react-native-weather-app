import React from 'react';
import { StyleSheet, View, Image, Text } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../actions'; //Import your actions

import FacebookButton from './FacebookButton';
import GoogleButton from './GoogleButton';
import AnonymButton from './AnonymButton';

import { strings } from '../../locales/i18n';
import Connection from '../Notifications/Connection';


class LoginPage extends React.Component {
    constructor(props) {
        super(props) 
        this.state = {
            renderFacebook: true,
            renderGoogle: true,
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.checkedFb !== this.props.checkedFb && this.props.checkedFb) {
            if(!this.props.facebookCached) {
                console.log(this.props.facebookCached);
                console.log("change here..");
                this.setState({ renderFacebook: false });
            }
        }
        if(prevProps.checkedGoo !== this.props.checkedGoo && this.props.checkedGoo) {
            if(!this.props.googleCached) {
                console.log("google change here..");
                this.setState({ renderGoogle: false });
            }
        }
    }

    render() {
        const renderSocial = this.state.renderFacebook || this.state.renderGoogle;
        let oneSocial;

        if(this.state.renderFacebook && this.state.renderGoogle) oneSocial = false;
        else if(this.state.renderFacebook || this.state.renderGoogle) oneSocial = true;
        else oneSocial = false;

        console.log("RENDER FACEBOOK -- " + this.state.renderFacebook);
        console.log("RENDER SOCIAL -- " + renderSocial);

        return (
            <View style={styles.container}>
                {!this.props.isConnected && <Connection data={true} /> }
                <View style={styles.imgContainer}>
                <Image source={require('../../assets/images/user-logo.png')}
                    style={{width:'70%', height:'70%', resizeMode: 'contain'}} />
                </View>
                <View style={styles.loginContainer}>
                    {renderSocial && <View style={oneSocial ? styles.oneSocialContainer : styles.socialContainer}>
                                        <Text>{strings('login.loginWith')}</Text>
                                        {this.state.renderFacebook && <FacebookButton />}
                                        {this.state.renderGoogle && <GoogleButton />}
                                    </View>}
                    {renderSocial && <Text>{strings('login.or')}</Text>}
                    <AnonymButton />
                </View>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        isConnected: state.connectionReducer.data,
        facebookCached: state.socialCachedReducer.facebook,
        googleCached: state.socialCachedReducer.google,
        checkedFb: state.socialCachedReducer.checkFb,
        checkedGoo: state.socialCachedReducer.checkGoo
    }
  }

  function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2'
    },
    imgContainer: {
        margin: 10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginContainer: {
        flex: 1,
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    socialContainer: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    oneSocialContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});