import React from 'react'
import { View, StyleSheet, Text } from 'react-native';

import { strings } from '../../locales/i18n';


export default class NoOfflineData extends React.Component {
    constructor(props) {
        super(props);
    }

    renderLoader = () => {
        return(
            <View style={styles.loaderContainer}>
                <ActivityIndicator size="large" color="#0000ff" /> 
            </View>
        );
    }
        
    
    render() {
        return (
            <View style={styles.noDataContainer}>
                <Text style={styles.text}>{strings('homePage.noOfflineData')}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    loaderContainer: {
        margin: 30
    },
    noDataContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '50%',
    },
    text: {
        flex: 1,
        fontSize: 30,
        fontWeight: 'bold',
    }
});



