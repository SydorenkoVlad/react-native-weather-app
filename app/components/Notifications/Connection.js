import React from 'react';
import { View, Text, StyleSheet, Button, Platform } from 'react-native';

import { strings } from '../../locales/i18n';


class Connection extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const renderData = this.props.data;
        let styleContainer;

        if(renderData) {
            if(Platform.OS === "ios") {
                styleContainer = styles.iOSContainer;
            }
            else {
                styleContainer = styles.relativeContainer;
            }
        }
        else {
            styleContainer = styles.fixedContainer;
        }

        return (
            <View style={styleContainer}>
                <View style={styles.text}>
                    <Text style={{ color: 'white' }}>{strings("homePage.noInternet")}</Text>
                </View>
                { this.props.close && 
                    <View style={styles.button}>
                        <Button
                            onPress={this.props.close}
                            title={strings("homePage.dismiss")}
                            color="blue" />
                    </View> }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fixedContainer: {
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        padding: 5
    },
    relativeContainer: {
        flexDirection: 'row',
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        // marginTop: 10,
        padding: 5
    },
    iOSContainer: {
        flexDirection: 'row',
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        padding: 5
    },
    text: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export default Connection;