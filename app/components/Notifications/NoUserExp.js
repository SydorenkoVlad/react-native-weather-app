import React from 'react'
import { View, StyleSheet, Text } from 'react-native';

import { strings } from '../../locales/i18n';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


export default class NoUserExp extends React.Component {
    constructor(props) {
        super(props);
    }

    renderLoader = () => {
        return(
            <View style={styles.loaderContainer}>
                <ActivityIndicator size="large" color="#0000ff" /> 
            </View>
        );
    }
        
    
    render() {
        return (
            <View style={styles.noDataContainer}>
                <Text style={styles.text}>{strings('homePage.noUserExp')}</Text>
                <Icon name="city" size={100}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    loaderContainer: {
        margin: 30
    },
    noDataContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '50%',
        marginLeft: '5%',
        marginRight: '5%',
    },
    text: {
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 25,
        fontWeight: 'bold',
    }
});



