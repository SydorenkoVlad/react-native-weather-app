import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import { strings } from '../../locales/i18n';


class NotFound extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.relativeContainer}>
                <Text style={{ color: 'white' }}>{strings('homePage.cityNotFound')}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    relativeContainer: {
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15
    }
});

export default NotFound;