import React from 'react';
import { StyleSheet, View, AsyncStorage } from 'react-native';
import firebase from 'react-native-firebase';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions'; //Import your actions

import { fetchTimeout } from '../helpers';

import Home from './HomePage/Home' //Import the component file

class MainContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            token: ''
        }
    }

    componentDidMount() {
        fetchTimeout('http://google.com', 2000)
            .then(() => {
                console.log("mainContainer ping.....");
                const connection = true;

                this.props.changeConnection(connection);
            })
            .catch((e) => {
                console.log("mainContainer ping Error " + e);
                const connection = false;

                this.props.changeConnection(connection);
            })

        firebase.messaging().getToken()
        .then(fcmToken => {
            console.log("FCM Token success");
            if (fcmToken) {
                this.setState({ token: fcmToken });
            } else {
                this.setState({ token: 'no token yet'});
            } 
        });
    }

    componentDidUpdate(prevProps) {
        if(prevProps.isConnected !== this.props.isConnected && !this.props.isConnected && !this.props.user) {
            let google = null, facebook = null;
            AsyncStorage.getAllKeys().then((result) => {
                result.map((item) => {
                    console.log(item);
                    if(item.indexOf('facebook') == 0) {
                        facebook = item; 
                    }
                    if(item.indexOf('google') == 0) {
                        google = item;
                    }
                })
            
                this.dispatchFacebook(facebook);
                this.dispatchGoogle(google);
            })
        }
    }

    dispatchFacebook(item) {
        if(item) {
            console.log("Found facebook account in cash");
            const stringIndex = item.indexOf(':'); 
            const cachedAccount = item.substring(9, stringIndex);
        
            this.props.dispatchFacebook(cachedAccount);
        }
        else this.props.checkCacheFacebook(true);
    }

    dispatchGoogle(item) {
        if(item) {
            console.log("Found google account in cash");
            const stringIndex = item.indexOf(':'); 
            const cachedAccount = item.substring(7, stringIndex);
            
            this.props.dispatchGoogle(cachedAccount);
        }
        else this.props.checkCacheGoogle(true);
    }

    render() {
        return (
            <View style={styles.container}>
                <Home />
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        isConnected: state.connectionReducer.data,
        facebookChecked: state.socialCachedReducer.checkFb,
        googleChecked: state.socialCachedReducer.checkGoo,
        user: state.userReducer.user,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ccffff',
        flex: 1
    }
})
