import city1 from './dataFolder/cities1';
import city2 from './dataFolder/cities2';
import city3 from './dataFolder/cities3';
import city4 from './dataFolder/cities4';
import city5 from './dataFolder/cities5';
import city6 from './dataFolder/cities6';
import city7 from './dataFolder/cities7';
import city8 from './dataFolder/cities8';
import city9 from './dataFolder/cities9';
import city10 from './dataFolder/cities10';


export function convertTempreature(kelvin) {
    let celcius = kelvin - 273.15;
    
    celcius = Math.round(celcius);
    celcius = celcius > 0 ? "+" + celcius : "-" + celcius;  
    celcius = celcius + "°";

    return celcius;
}

export function parseAdditionalInfo(data, date) {

    let requestDay = date; 

    let parsedData = {};            
    let tempDay = {};

    const city = data.city.name;
    const id = data.city.id;

    parsedData.id = id;
    parsedData.city = city;

    const list = data.list;

    list.forEach(element => {
        const date = element.dt_txt;
        const month = date.substr(5, 2);
        const shortDate = month + "." + date.substr(8, 2);
        

        if(requestDay == shortDate) {

            parsedData[requestDay] = tempDay;

            const time = date.substr(11, 5);
            const minTempreature = convertTempreature(element.main.temp_min);
            const maxTempreature = convertTempreature(element.main.temp_max);
            const icon = `http://openweathermap.org/img/w/${element.weather[0].icon}.png`;

            tempDay[time] = {
                tempreature: convertTempreature(element.main.temp),
                minTempreature: minTempreature == maxTempreature ? '' : minTempreature,
                maxTempreature: minTempreature == maxTempreature ? '' : maxTempreature,
                pressure: element.main.pressure,
                humidity: element.main.humidity,
                weatherCondition: element.weather[0].main,
                description: element.weather[0].description,
                icon: icon,
                clouds: element.clouds.all + "%", 
                wind: element.wind.speed,
                snow: element.snow ? checkComplexValue(element.snow, 'Snowy') : '',
                rain: element.rain ? checkComplexValue(element.rain, 'Raining') : ''
            };

        }                
    });

    return parsedData;
}

function checkComplexValue(value, description) {
    return Object.keys(value).length ? Math.round(value["3h"] * 100) / 100 + ' mm' : description;
}

export function checkTodayDate(data) {
    let today = new Date();
    today = today.toJSON().substr(0,10);

    console.log("Today: " + today);

    let date = data.list[0].dt_txt;
    date = date.substr(0,10);
    
    console.log("From data: " + date );

    if(today == date) {
        return true;
    }

    return false; 
}

export function checkDateOfflineMode(data) {
    let today = new Date();
    today.setDate(today.getDate() - 4);

    today = today.toJSON().substr(0,10);

    console.log("Today: " + today);

    let date = data.list[0].dt_txt;
    date = date.substr(0,10);
    
    console.log("From data: " + date );

    console.log("year " + date.substr(0, 4));
    console.log("month " + date.substr(5, 2));
    console.log("day " + date.substr(8, 2));

    if(date.substr(0,4) == today.substr(0,4)) {
        console.log(date.substr(0, 4) + " Same year");
        // same year

        if(date.substr(5,2) == today.substr(5,2)) {
            // same month
            let difference = +date.substr(8, 2) - +today.substr(8, 2);
            if(Math.abs(difference) <= 4) {
                console.log("One month -- valid data -- ok");
                return true;
            } 
        }
        else if(+today.substr(5, 2) - +date.substr(5, 2) == 1 ) {
            // different months
            let difference = +date.substr(8, 2) - +today.substr(8, 2); 
            if(difference >= 24) {
                console.log("Different month -- valid data -- ok");
                return true;
            }
        }
        return true;
    }

    return false;
}

export function searchCity(cityId, name) {
    let data = [city1, city2, city3, city4, city5,
                city6, city7, city8, city9, city10];

    for(let i = 0; i < data.length; i++) {

        
        let city = data[i].filter((item) => {
            if(name) {
                return item.name == name;    
            }
            else {
                return item.id == cityId;
            } 
        });

        if(city[0]) {
            return name ? city[0].id : city[0].name;
        } 
    }
}

export function fetchTimeout(url, timeout) {
    return Promise.race([
        fetch(url),
        new Promise((_, reject) =>
            setTimeout(() => reject(new Error('timeout')), timeout)
        )
    ]);
}




